require('dotenv').config();
const { leerInput,inquirerMenu, pausa ,listarLugares } = require('./helpers/inquirer');
const Busquedas = require('./models/busquedas');

const main = async () =>{
    const busquedas = new Busquedas();
    let opt;

    do {
        opt = await inquirerMenu();
        //console.log({opt});
        switch (opt){
            case 1 :
                //mostrar lugar
                const termino = await leerInput('Ciudad: ');
                //buscar los lugares
                const lugares = await busquedas.ciudad(termino);
                //console.log(lugares);
                //selecionar el lugar
                const id = await listarLugares(lugares);
                //console.log({id});
                if (id === '0') continue;

                const lugarSel = lugares.find( l => l.id === id);
                
                busquedas.agregarHistorial(lugarSel.nombre);
                //console.log(lugarSel);
                const clima = await busquedas.climaLugar(lugarSel.lat,lugarSel.lng);
                //clima
                //mostrar resultado
                console.clear();
                console.log('\n Información de la ciudad\n'.green);
                console.log('Ciudad:', lugarSel.nombre);
                console.log('Lat: ',lugarSel.lat);
                console.log('Lng: ', lugarSel.lng);
                console.log('Temperatura: ',clima.temp);
                console.log('Mínima: ',clima.min);
                console.log('Máxima: ',clima.max);
                console.log('Como esta el clima: ',clima.desc);

            break;
            case 2:
                busquedas.historialCapitalizado.forEach((lugar, i)=>{
                    const idx = `${ i + 1 }.`.green;
                    console.log(`${idx} ${lugar}`);
                });
                /*busquedas.historial.forEach((lugar, i)=>{
                    const idx = `${ i + 1 }.`.green;
                    console.log(`${idx} ${lugar}`);
                });*/
            break;
        }
        await pausa();
    } while (opt !== 0);
}

main();